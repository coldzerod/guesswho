package com.norahm.guesswho.ui.activities;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.norahm.guesswho.R;
import com.norahm.guesswho.model.CastrolPlayer;
import com.norahm.guesswho.services.CommunicateService;
import com.norahm.guesswho.utils.UserData;

public class PlayNameActivity extends SherlockActivity {

	private CommunicateService imService;

	private String[] options = new String[4];
	private int posInLevel, playerId;
	private int pos_correctans;
	private String picid;
	private Boolean flag = false;
	private int levelnum = 0;
	private int numTries = 0;
	private MediaPlayer mp;
	private String loading_Messege;
	UserData userData = UserData.getInstance();

	private TextView timerValue;

	private long startTime = 0L;

	private Handler customHandler = new Handler();

	long timeInMilliseconds = 0L;
	long timeSwapBuff = 0L;
	long updatedTime = 0L;

	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the service object we can use to
			// interact with the service. Because we have bound to a explicit
			// service that we know is running in our own process, we can
			// cast its IBinder to a concrete class and directly access it.
			imService = ((CommunicateService.IMBinder) service).getService();
			flag = true;
			// Log.v("imService", "Connected");
			if (imService.isUserAuthenticated() == true) {
				Intent i = new Intent(PlayNameActivity.this,
						PlayNameActivity.class);
				startActivity(i);
				PlayNameActivity.this.finish();
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			// Because it is running in our same process, we should never
			// see this happen.
			imService = null;
			Toast.makeText(PlayNameActivity.this,
					"The service has disconnected", Toast.LENGTH_SHORT).show();
		}
	};

	private AdView adView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play_position);

		posInLevel = 1;
		levelnum = getIntent().getExtras().getInt("numLvL");// ("Level", level);
		numTries = getIntent().getExtras().getInt("numTries");
		levelnum++;

		enableAdMob();

		loading_Messege = getResources().getString(R.string.loading_level);

		LoadTask loadTask = new LoadTask();
		loadTask.execute(levelnum, posInLevel);

		((TextView) findViewById(R.id.numberLevel)).setText(String
				.valueOf(levelnum));

		((TextView) findViewById(R.id.numberTries)).append(String
				.valueOf(numTries));

		mp = MediaPlayer.create(this, R.drawable.correct);

		timerValue = (TextView) findViewById(R.id.timerValue);

		startTime = SystemClock.uptimeMillis();
		customHandler.postDelayed(updateTimerThread, 0);
	}

	private class LoadTask extends AsyncTask<Integer, Integer, Boolean> {

		ProgressDialog pdLoading;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pdLoading = new ProgressDialog(PlayNameActivity.this);
			// this method will be running on UI thread
			loading_Messege = getResources().getString(R.string.loading_level);
			pdLoading.setMessage("\t" + loading_Messege);
			pdLoading.show();

		}

		@Override
		protected Boolean doInBackground(Integer... params) {
			Boolean res = false;

			while (!flag) {
				if (flag)
					break;
			}
			res = LoadLevelData(params[0], params[1]);

			return res;
		}

		@Override
		protected void onPostExecute(Boolean result) {

			// this method will be running on UI thread

			pdLoading.dismiss();

			if (!result)
				Toast.makeText(PlayNameActivity.this, "Error Loading Level!",
						Toast.LENGTH_SHORT).show();
			else {
				ImageView mImage = (ImageView) findViewById(R.id.imageView1);

				Button btn = (Button) findViewById(R.id.btn1);
				btn.setText(options[0]);
				btn.setVisibility(View.VISIBLE);
				
				btn = (Button) findViewById(R.id.btn2);
				btn.setText(options[1]);
				btn.setVisibility(View.VISIBLE);
				
				btn = (Button) findViewById(R.id.btn3);
				btn.setText(options[2]);
				btn.setVisibility(View.VISIBLE);
				
				btn = (Button) findViewById(R.id.btn4);
				btn.setText(options[3]);
				btn.setVisibility(View.VISIBLE);
				
				try {
					// get input stream
					InputStream ims = getAssets().open(
							"Pics/" + playerId + "-" + picid + ".jpg");
					// load image as Drawable
					Drawable d = Drawable.createFromStream(ims, null);
					// set image to ImageView
					mImage.setImageDrawable(d);
				} catch (IOException ex) {
					ex.printStackTrace();
					return;
				}

			}
		}
	}

	public Boolean LoadLevelData(int level, int posInLevel) {

		Random rand = new Random();
		options = new String[4];
		playerId = ((level * 10) - (10)) + posInLevel;

		CastrolPlayer cp = new CastrolPlayer();
		if (imService == null) {
			// Log.v("problem", "imService");
		} else if (imService.Players == null || imService.Players.size() <= 0) {
			// Log.v("problem", "Players");
			imService.LoadPlayersData();
		} else if (playerId < 0) {
			// Log.v("problem", "playerId");
		}
		cp = imService.Players.get(playerId);
		int pos = rand.nextInt(4);
		int otherpos;
		int otherId;
		options[pos] = cp.Name;
		picid = cp.pic_id;
		pos_correctans = pos;
		for (int i = 0; i < 3; i++) {
			otherpos = rand.nextInt(4);
			while (otherpos == pos || (options[otherpos]) != null) {
				otherpos = rand.nextInt(4);
			}
			otherId = rand.nextInt(608);
			while (otherId == playerId) {
				otherId = rand.nextInt(608);
			}
			cp = imService.Players.get(otherId);
			options[otherpos] = cp.Name;
		}

		return true;
	}

	public void ButtonOnClick(View v) {
		Boolean result = false;
		switch (v.getId()) {
		case R.id.btn1:
			result = compare_click(0, pos_correctans);
			break;
		case R.id.btn2:
			result = compare_click(1, pos_correctans);
			break;
		case R.id.btn3:
			result = compare_click(2, pos_correctans);
			break;
		case R.id.btn4:
			result = compare_click(3, pos_correctans);
			break;
		case R.id.hintbutton:
			timeSwapBuff += timeInMilliseconds;
			customHandler.removeCallbacks(updateTimerThread);

			showHintDialog(this);
			customHandler.postDelayed(updateTimerThread, 0);
			return;
		}

		int maxlvl = userData.getMaxUnlockedLevelname();
		if (result == true) {
			int res = ((levelnum * 10) - 10) + posInLevel + 1;
			if (res < imService.Players.size() && posInLevel < 10) {
				posInLevel++;
				LoadTask loadTask = new LoadTask();
				loadTask.execute(levelnum, posInLevel);
			}
			else if(res+1 >= imService.Players.size())
			{
				Toast.makeText(PlayNameActivity.this,
						R.string.Congratsfinal,
						Toast.LENGTH_LONG).show();
				
				userData.unlockNextLevelNAME();
				userData.incrementNumofTriesName(levelnum - 1);
				userData.incrementPointsLevelname((int)(updatedTime/1000), userData.getNumofTriesName(levelnum-1));

				finish();
			}
			else {
				/*
				 * InputStream stream = null; try { stream =
				 * getAssets().open("ballsmiles.gif"); } catch (IOException e) {
				 * e.printStackTrace(); }
				 * 
				 * GifDecoderView view = new GifDecoderView(this, stream);
				 * 
				 * LinearLayout.LayoutParams layoutParams = new
				 * LinearLayout.LayoutParams(62, 61);
				 * layoutParams.gravity=Gravity.CENTER;
				 * view.setLayoutParams(layoutParams); // b.gravity
				 * =Gravity.CENTER; //view.setLayoutParams(b);
				 * setContentView(view);
				 */
				mp.start();

				if (levelnum >= maxlvl) {
					// if (levelnum < 5)
					{
						Toast.makeText(PlayNameActivity.this,
								R.string.Congrats,
								Toast.LENGTH_SHORT).show();
						
						finish();

					}
					/*
					 * else { imService.showRateDialog(this,
					 * Integer.toString(levelnum+1)); }
					 */
					userData.unlockNextLevelNAME();
					userData.incrementNumofTriesName(levelnum - 1);
					userData.incrementPointsLevelname((int)(updatedTime/1000), userData.getNumofTriesName(levelnum-1));
				}
			}
		} else {
			Toast.makeText(PlayNameActivity.this, R.string.InCorrect,
					Toast.LENGTH_SHORT).show();
			if (levelnum >= maxlvl) {
				userData.incrementNumofTriesName(levelnum - 1);
			}
			finish();
		}
	}

	private Boolean compare_click(int butpos, int corre_pos) {
		if (butpos == corre_pos)
			return true;
		return false;
	}

	protected void enableAdMob() {

		adView = (AdView) findViewById(R.id.adView_name);
		adView.setVisibility(View.INVISIBLE);

		adView.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
				adView.setVisibility(View.VISIBLE);
			}
		});
		AdRequest.Builder ads = new AdRequest.Builder();
		ads.addTestDevice("03B293FAF6DFB2307FF7550EA6CA173B");
		AdRequest adRequest = ads.build();
		adView.loadAd(adRequest);
	}

	@Override
	protected void onPause() {
		unbindService(mConnection);
		if (adView != null) {
			adView.pause();
		}
		// Log.v("Loggin", "onPause");
		super.onPause();
	}

	@Override
	protected void onRestart() {

		// Log.v("Loggin", "onRestart");
		super.onRestart();
	}

	@Override
	protected void onResume() {
		bindService(
				new Intent(PlayNameActivity.this, CommunicateService.class),
				mConnection, Context.BIND_AUTO_CREATE);
		if (adView != null) {
			adView.resume();
		}
		// Log.v("Loggin", "onResume");

		super.onResume();
	}

	@Override
	protected void onDestroy() {
		if (adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}

	public void showHintDialog(final Context mContext) {
		final Dialog dialog = new Dialog(mContext);
		dialog.setTitle("Hints Options");

		LinearLayout ll = new LinearLayout(mContext);
		ll.setOrientation(LinearLayout.VERTICAL);
		int points = userData.getPointsLevelname();

		final int poscorrectans = pos_correctans + 1;
		TextView tv = new TextView(mContext);
		tv.setText("You currently have " + points +" Points!");
		tv.setTextColor(getResources().getColor(android.R.color.white));
		tv.setWidth(340);
		tv.setPadding(4, 0, 4, 10);
		ll.addView(tv);

		Button b1 = new Button(mContext);
		b1.setText(R.string.hint1option);
		b1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				int points = userData.getPointsLevelname();
				if (points > 30) {
					if (poscorrectans == 1) {
						findViewById(R.id.btn2).setVisibility(View.INVISIBLE);
						findViewById(R.id.btn3).setVisibility(View.INVISIBLE);
						findViewById(R.id.btn4).setVisibility(View.INVISIBLE);
					} else if (poscorrectans == 2) {
						findViewById(R.id.btn1).setVisibility(View.INVISIBLE);
						findViewById(R.id.btn3).setVisibility(View.INVISIBLE);
						findViewById(R.id.btn4).setVisibility(View.INVISIBLE);
					} else if (poscorrectans == 3) {
						findViewById(R.id.btn1).setVisibility(View.INVISIBLE);
						findViewById(R.id.btn2).setVisibility(View.INVISIBLE);
						findViewById(R.id.btn4).setVisibility(View.INVISIBLE);
					} else if (poscorrectans == 4) {
						findViewById(R.id.btn2).setVisibility(View.INVISIBLE);
						findViewById(R.id.btn3).setVisibility(View.INVISIBLE);
						findViewById(R.id.btn1).setVisibility(View.INVISIBLE);
					}
					
					userData.decrementPointsLevelname(30);
				}
				else
				{
					Toast.makeText(PlayNameActivity.this, R.string.nopoints,
							Toast.LENGTH_SHORT).show();
				}
				
				dialog.dismiss();
			}
		});
		ll.addView(b1);

		Button b2 = new Button(mContext);
		b2.setText(R.string.hint2option);
		b2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				int points = userData.getPointsLevelname();
				if (points > 15) {
					if (poscorrectans == 1) {
						findViewById(R.id.btn2).setVisibility(View.INVISIBLE);
						findViewById(R.id.btn4).setVisibility(View.INVISIBLE);
					} else if (poscorrectans == 2) {
						findViewById(R.id.btn1).setVisibility(View.INVISIBLE);
						findViewById(R.id.btn3).setVisibility(View.INVISIBLE);
					} else if (poscorrectans == 3) {
						findViewById(R.id.btn2).setVisibility(View.INVISIBLE);
						findViewById(R.id.btn4).setVisibility(View.INVISIBLE);
					} else if (poscorrectans == 4) {
						findViewById(R.id.btn3).setVisibility(View.INVISIBLE);
						findViewById(R.id.btn1).setVisibility(View.INVISIBLE);
					}
					userData.decrementPointsLevelname(15);

				}
				else
				{
					Toast.makeText(PlayNameActivity.this, R.string.nopoints,
							Toast.LENGTH_SHORT).show();
				}
				

				dialog.dismiss();
			}
		});
		ll.addView(b2);

		Button b3 = new Button(mContext);
		b3.setText("Cancel");
		b3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				dialog.dismiss();
			}
		});
		ll.addView(b3);

		dialog.setContentView(ll);
		dialog.show();
	}

	private Runnable updateTimerThread = new Runnable() {

		public void run() {

			timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

			updatedTime = timeSwapBuff + timeInMilliseconds;

			int secs = (int) (updatedTime / 1000);
			int mins = secs / 60;
			secs = secs % 60;
			int milliseconds = (int) (updatedTime % 1000);
			timerValue.setText("" + mins + ":" + String.format("%02d", secs)
					+ ":" + String.format("%03d", milliseconds));
			customHandler.postDelayed(this, 0);
		}

	};
}
