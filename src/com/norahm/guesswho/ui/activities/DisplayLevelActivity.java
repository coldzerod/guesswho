package com.norahm.guesswho.ui.activities;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.norahm.guesswho.R;
import com.norahm.guesswho.services.CommunicateService;
import com.norahm.guesswho.ui.adapters.DisplayLevelAdapter;
import com.norahm.guesswho.ui.adapters.DisplayLevelAdapter.RowItem;
import com.norahm.guesswho.utils.UserData;

public class DisplayLevelActivity extends SherlockActivity {

	private CommunicateService imService;

	private EasyTracker easyTracker = null;

	private String load_Game;

	private LoadTask loadTask;
	GridView grid;
	String[] Levels = new String[61];
	Integer[] images = new Integer[61];
	String[] fullTeamsNames = { "Australia", "Honduras", "Croatia", "Ecuador",
			"Greece", "Uruguay", "France", "South Korea", "Ghana",
			"Switzerland", "Chile", "Italy", "Japan", "Mexico", "Algeria",
			"Cameroon", "C�te d�Ivoire", "Argentina", "Iran", "Colombia",
			"Portugal", "Germany", "Netherlands", "North America",
			"Costa Rica", "Russia", "Nigeria", "Brazil", "England", "Belgium",
			"Spain", "Bosnia and Herzegovina" };
	int typeGame = 0;
	UserData userData = UserData.getInstance();
	private int maxlvl;
	private int numTries;
	ListView listView;
	List<RowItem> rowItems;
	private String level_lang;
	private Boolean flag = false;

	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the service object we can use to
			// interact with the service. Because we have bound to a explicit
			// service that we know is running in our own process, we can
			// cast its IBinder to a concrete class and directly access it.
			imService = ((CommunicateService.IMBinder) service).getService();
			flag = true;
			//Log.v("imService", "Connected");
			if (imService.isUserAuthenticated() == true) {
				Intent i = new Intent(DisplayLevelActivity.this,
						PlayNameActivity.class);
				startActivity(i);
				DisplayLevelActivity.this.finish();
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			// Because it is running in our same process, we should never
			// see this happen.
			imService = null;
			Toast.makeText(DisplayLevelActivity.this,
					"The service has disconnected", Toast.LENGTH_SHORT).show();
		}
	};

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_levels);

		typeGame = getIntent().getExtras().getInt("typeGame");
		grid = (GridView) findViewById(R.id.levelsView);

		easyTracker = EasyTracker.getInstance(DisplayLevelActivity.this);

		load_Game = getResources().getString(R.string.loading_game);

	}

	private Boolean displayData() {

		level_lang = getResources().getString(R.string.game_level);
		if (typeGame == 0)
			maxlvl = userData.getMaxUnlockedLevelpos();
		else if (typeGame == 1)
			maxlvl = userData.getMaxUnlockedLevelname();
		else {
			maxlvl = userData.getMaxUnlockedLevelteam();
		}

		if (typeGame <= 1) {
			for (int i = 1; i <= 61; i++) {

				Levels[i - 1] = level_lang + " " + i;
				if (i <= maxlvl)
					images[i - 1] = R.drawable.unlock;
				else
					images[i - 1] = R.drawable.lock;
			}
		} else {
			int level_nums = imService.teams.length;
			Levels = new String[level_nums];
			for (int i = 1; i <= level_nums; i++) {
				Levels[i - 1] = fullTeamsNames[i - 1] + " ("
						+ imService.teams[i - 1] + ")";
				if (i <= maxlvl)
					images[i - 1] = R.drawable.unlock;
				else
					images[i - 1] = R.drawable.lock;
			}

		}

		if (rowItems != null)
			rowItems.clear();

		rowItems = new ArrayList<RowItem>();
		for (int i = 0; i < Levels.length; i++) {
			RowItem item = new RowItem(images[i], Levels[i]);
			rowItems.add(item);
		}
		ArrayAdapter<RowItem> adapter = new DisplayLevelAdapter(this,
				R.layout.adapter_display_level, rowItems);

		grid.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		// grid.setTextAlignment(View.TEXT_ALIGNMENT_INHERIT);
		grid.setOnItemClickListener(new OnItemClickListener() {

			@SuppressWarnings("rawtypes")
			public void onItemClick(AdapterView parent, View v, int position,
					long id) {

				// Toast.makeText(getApplicationContext(),((TextView)
				// v).getText(), Toast.LENGTH_SHORT).show();

				Intent startLevl;
				String action, event = "level", click_name = Integer
						.toString(position);
				if (typeGame == 0) {
					maxlvl = userData.getMaxUnlockedLevelpos();
					numTries = userData.getNumofTriesPos(position);
					action = "PlayByName";
					startLevl = new Intent(getApplicationContext(),
							PlayPositionActivity.class);
				} else if (typeGame == 1) {
					maxlvl = userData.getMaxUnlockedLevelname();
					numTries = userData.getNumofTriesName(position);
					action = "PlayByPos";
					startLevl = new Intent(getApplicationContext(),
							PlayNameActivity.class);
				} else {
					maxlvl = userData.getMaxUnlockedLevelteam();
					numTries = userData.getNumofTriesTeam(position);
					action = "PlayByTeam";
					startLevl = new Intent(getApplicationContext(),
							PlayByTeamActivity.class);
				}

				// startLevl.putExtra("Level", ((TextView) v).getText());

				if ((position + 1) <= maxlvl) {
					startLevl.putExtra("numLvL", position);
					startLevl.putExtra("numTries", numTries);
					startActivity(startLevl);

					easyTracker.send(MapBuilder.createEvent(action, event,
							click_name, null).build());
				} else
					Toast.makeText(DisplayLevelActivity.this,
							"You did not pass the levels before!",
							Toast.LENGTH_SHORT).show();

			}

		});

		return true;
	}

	private class LoadTask extends AsyncTask<Void, Integer, Boolean> {

		ProgressDialog pdLoading = new ProgressDialog(DisplayLevelActivity.this);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// this method will be running on UI thread
			pdLoading.setMessage("\t" + load_Game);
			pdLoading.show();
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			Boolean res = true;

			while (!flag) {
				if (flag)
					break;
			}

			while (!imService.isDataLoaded) {
				if (imService.isDataLoaded)
					break;
			}
			return res;
		}

		@Override
		protected void onPostExecute(Boolean result) {

			// this method will be running on UI thread

			displayData();

			pdLoading.dismiss();

			if (!result)
				Toast.makeText(DisplayLevelActivity.this,
						"Error Loading Game!", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onPause() {
		unbindService(mConnection);
		super.onPause();
	}

	@Override
	protected void onResume() {
		bindService(new Intent(DisplayLevelActivity.this,
				CommunicateService.class), mConnection,
				Context.BIND_AUTO_CREATE);
		loadTask = new LoadTask();
		loadTask.execute();
		super.onResume();
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}
}
